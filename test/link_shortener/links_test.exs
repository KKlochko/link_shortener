defmodule LinkShortener.LinksTest do
  use LinkShortener.DataCase

  alias LinkShortener.Links

  @create_attrs %{
    name: "some link name",
    url: "https://gitlab.com/KKlochko/link_shortener",
    shorten: "git_repo",
  }
  @create_generated_attrs %{
    name: "some link name",
    url: "https://gitlab.com/KKlochko/link_shortener",
  }
  @update_attrs %{
    name: "some updated link name",
    url: "https://gitlab.com/KKlochko/link_shortener2",
    shorten: "new_git_repo",
  }
  @invalid_attrs %{
    name: nil,
    url: nil,
    shorten: nil,
  }

  describe "links" do
    alias LinkShortener.Links.Link

    import LinkShortener.LinksFixtures

    test "new_one/1 returns the changeset" do
      assert %Ecto.Changeset{} = Links.new_one()
    end

    test "create_one/1 with valid data creates a link" do
      assert {:ok, %Link{} = link} = Links.create_one(@create_generated_attrs)
      assert link.name == "some link name"
      assert link.url == "https://gitlab.com/KKlochko/link_shortener"
      assert String.length(link.shorten) == 10
    end

    test "create_one/2 with valid data creates a link" do
      assert {:ok, %Link{} = link} = Links.create_one(@create_generated_attrs, 5)
      assert link.name == "some link name"
      assert link.url == "https://gitlab.com/KKlochko/link_shortener"
      assert String.length(link.shorten) == 5
    end

    test "insert_one/1 with valid data creates a link" do
      assert {:ok, %Link{} = link} = Links.insert_one(@create_attrs)
      assert link.name == "some link name"
      assert link.url == "https://gitlab.com/KKlochko/link_shortener"
      assert link.shorten == "git_repo"
    end

    test "insert_one/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Links.insert_one(@invalid_attrs)
    end

    test "get_one!/1 returns the link with given id" do
      link = link_fixture()
      assert Links.get_one!(link.id) == link
    end

    test "get_one_by!/1 returns the link with given shorten" do
      link = link_fixture()
      assert Links.get_one_by(%{shorten: link.shorten}) == link
    end

    test "get_one_by_shorten!/1 returns the link with given shorten" do
      link = link_fixture()
      assert Links.get_one_by_shorten(link.shorten) == link
    end

    test "get_all/0 returns all links" do
      link = link_fixture()
      assert Links.get_all() == [link]
    end

    test "edit_one/1 with valid data returns the changeset" do
      link = link_fixture()
      assert %Ecto.Changeset{} = Links.edit_one(link)
    end

    test "update_one/2 with valid data updates the link" do
      link = link_fixture()

      assert {:ok, %Link{} = link} = Links.update_one(link, @update_attrs)
      assert link.name == "some updated link name"
      assert link.url == "https://gitlab.com/KKlochko/link_shortener2"
      assert link.shorten == "new_git_repo"
    end

    test "update_link/2 with invalid data returns error changeset" do
      link = link_fixture()
      assert {:error, %Ecto.Changeset{}} = Links.update_one(link, @invalid_attrs)
      assert link == Links.get_one!(link.id)
    end

    test "delete_one/1 deletes the link" do
      link = link_fixture()
      assert {:ok, %Link{}} = Links.delete_one(link)
      assert_raise Ecto.NoResultsError, fn -> Links.get_one!(link.id) end
    end
  end
end
