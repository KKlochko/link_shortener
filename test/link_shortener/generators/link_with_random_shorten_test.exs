defmodule LinkShortener.Generators.LinkWithRandomShortenTest do
  use ExUnit.Case, async: true
  
  alias LinkShortener.Generators.LinkWithRandomShorten

  @name "Some name"
  @attrs %{
    name: @name
  }
  
  test "generate/1 returns random safe string with 10 as length" do
    assert %{
      name: @name,
      shorten: shorten
    } = LinkWithRandomShorten.generate_one(@attrs)

    assert String.length(shorten) == 10
  end

  test "generate/2 returns random safe string with 5 as length" do
    expected_length = 5

    assert %{
      name: @name,
      shorten: shorten
    } = LinkWithRandomShorten.generate_one(@attrs, expected_length)

    assert String.length(shorten) == expected_length
  end

  test "generate/3 returns random safe string using a lambda function" do
    random_generator = fn _ -> "random_shorten" end
    expected_shorten = random_generator.(14) 
    expected_length = expected_shorten |> String.length()

    assert %{
      name: @name,
      shorten: shorten
    } = LinkWithRandomShorten.generate_one(@attrs, expected_length, random_generator)

    assert String.length(shorten) == expected_length
    assert shorten == expected_shorten
  end
end
