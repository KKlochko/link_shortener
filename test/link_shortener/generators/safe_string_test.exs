defmodule LinkShortener.Generators.SafeStringTest do
  use ExUnit.Case, async: true

  alias LinkShortener.Generators.SafeString

  @lengths [1, 2, 5, 10]
  
  test "generate/1 returns random safe string with same length" do
    for expected_length <- @lengths do
      length = expected_length
      |> SafeString.generate()
      |> String.length()
      
      assert length = expected_length
    end
  end
end
