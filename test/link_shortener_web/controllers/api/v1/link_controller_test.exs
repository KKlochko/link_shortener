defmodule LinkShortenerWeb.Api.V1.LinkControllerTest do
  use LinkShortenerWeb.ConnCase

  import LinkShortener.LinksFixtures
  import LinkShortener.AccountsFixtures

  alias LinkShortener.Links.Link
  alias LinkShortener.Links

  @create_attrs %{
    name: "some link name",
    url: "https://gitlab.com/KKlochko/link_shortener",
    shorten: "git_repo",
  }
  @update_attrs %{
    name: "some updated link name",
    url: "https://gitlab.com/KKlochko/link_shortener2",
    shorten: "new_git_repo",
  }
  @invalid_attrs %{
    name: nil,
    url: nil,
    shorten: nil,
  }

  setup %{conn: conn} do
    %{token: token} = create_user_token()

    conn = conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer #{token}")

    {:ok, conn: conn}
  end

  describe "index" do
    test "lists all links", %{conn: conn} do
      conn = get(conn, Routes.v1_link_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create link" do
    test "renders link when data is valid", %{conn: conn} do
      conn = post(conn, Routes.v1_link_path(conn, :create), link: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.v1_link_path(conn, :show, id))

      assert %{
               "id" => ^id,
	       "name" => "some link name",
	       "url" => "https://gitlab.com/KKlochko/link_shortener",
	       "shorten" => "git_repo",
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.v1_link_path(conn, :create), link: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update link" do
    setup [:create_link]

    test "renders link when data is valid", %{conn: conn, link: %Link{id: id} = link} do
      conn = put(conn, Routes.v1_link_path(conn, :update, link), link: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.v1_link_path(conn, :show, id))

      assert %{
               "id" => ^id,
	       "name" => "some updated link name",
	       "url" => "https://gitlab.com/KKlochko/link_shortener2",
	       "shorten" => "new_git_repo",
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, link: link} do
      conn = put(conn, Routes.v1_link_path(conn, :update, link), link: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete link" do
    setup [:create_link]

    test "deletes chosen link", %{conn: conn, link: link} do
      conn = delete(conn, Routes.v1_link_path(conn, :delete, link))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.v1_link_path(conn, :show, link))
      end
    end
  end

  defp create_user_token() do
    token = user_token_fixture()
    %{token: token}
  end

  defp create_link(_) do
    link = link_fixture()
    %{link: link}
  end
end
