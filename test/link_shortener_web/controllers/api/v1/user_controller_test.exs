defmodule LinkShortenerWeb.Api.V1.UserControllerTest do
  use LinkShortenerWeb.ConnCase

  import LinkShortener.AccountsFixtures

  alias LinkShortener.Accounts.User

  @create_attrs %{
    email: "user@mail.com",
    password: "some password"
  }

  @update_attrs %{
    email: "some updated email",
    password: "some updated password"
  }

  @invalid_password_attrs %{
    email: "user@mail.com",
    password: ""
  }

  @invalid_attrs %{
    email: nil,
    encrypted_password: nil
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create user with sign up" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.v1_user_path(conn, :create), user: @create_attrs)
      assert %{
	"email" => "user@mail.com",
	"token" => token
      } = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.v1_user_path(conn, :create), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "user sign in" do
    setup [:create_user]

    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.v1_user_path(conn, :signin), @create_attrs)

      assert %{
	"email" => email,
	"token" => token,
      } = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.v1_user_path(conn, :signin), @invalid_password_attrs)
      assert "Unauthorized" == json_response(conn, 401)
    end
  end

  defp create_user(_) do
    user = user_fixture(@create_attrs)
    %{user: user}
  end
end
