defmodule LinkShortenerWeb.PageControllerTest do
  use LinkShortenerWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Usage"
  end
end
