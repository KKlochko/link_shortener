defmodule LinkShortener.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LinkShortener.Accounts` context.
  """

  alias LinkShortener.Accounts
  alias LinkShortener.Accounts.User
  alias LinkShortenerWeb.Auth.Guardian

  @doc """
  Generate a unique user email.
  """
  def unique_user_email, do: "user#{System.unique_integer()}@example.com"
  def valid_user_password, do: "hello world!"

  def valid_user_attributes(attrs \\ %{}) do
    Enum.into(attrs, %{
      email: unique_user_email(),
      password: valid_user_password()
    })
  end

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> valid_user_attributes()
      |> LinkShortener.Accounts.register_user()

    user
  end

  def user_token_fixture(attrs \\ %{}) do
    user_params = %{
      email: "user@mail.com",
      password: "some password"
    }

    {:ok, %User{} = user} = Accounts.register_user(user_params)
    {:ok, token, _claims} = Guardian.encode_and_sign(user)

    token
  end

  def extract_user_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end
end
