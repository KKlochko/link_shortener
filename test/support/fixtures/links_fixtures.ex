defmodule LinkShortener.LinksFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LinkShortener.Links` context.
  """

  alias LinkShortener.Links

  @doc """
  Generate a link.
  """
  def link_fixture(attrs \\ %{}) do
    {:ok, link} =
      attrs
      |> Enum.into(%{
        name: "some name",
	url: "https://gitlab.com/KKlochko/link_shortener",
	shorten: "api-article",
      })
      |> Links.create_one()

    link
  end
end
