defmodule LinkShortener.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :name, :string
      add :url, :string
      add :shorten, :string

      timestamps()
    end

    create unique_index(:links, [:shorten])
  end
end
