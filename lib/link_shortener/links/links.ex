defmodule LinkShortener.Links do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias LinkShortener.Repo
  alias LinkShortener.Links.Link
  alias LinkShortener.Generators.LinkWithRandomShorten, as: LinkGenerator

  def new_one(), do: Link.changeset(%Link{}, %{})

  def create_one(attrs, length \\ 10, generator \\ &LinkGenerator.generate_one/2) do
    generator.(attrs, length)
    |> insert_one()
  end

  def insert_one(attrs) do
    %Link{}
    |> Link.changeset(attrs)
    |> Repo.insert()
  end

  def get_one!(id) do
    Repo.get!(Link, id)
  end

  def get_one_by(attrs) do
    Repo.get_by(Link, attrs)
  end

  def get_one_by_shorten(shorten) do
    get_one_by(%{shorten: shorten})
  end

  def get_all() do
    from(Link)
    |> Repo.all()
  end

  def get_all(opts) do
    from(Link)
    |> Repo.all()
  end

  def edit_one(%Link{} = link) do
    link
    |> Link.changeset(%{})
  end

  def update_one(%Link{} = link, changes) do
    link
    |> Link.changeset(changes)
    |> Repo.update()
  end

  def delete_one(%Link{} = link), do: Repo.delete(link)
end
