defmodule LinkShortener.Links.Link do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  schema "links" do
    field :name, :string
    field :url, :string
    field :shorten, :string

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:name, :url, :shorten])
    |> validate_required([:url, :shorten])
    |> unique_constraint(:shorten)
  end
end
