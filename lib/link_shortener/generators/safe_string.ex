defmodule LinkShortener.Generators.SafeString do
  @moduledoc """
  This module provides a generator.
  """

  @doc """
  Generate a random string with the length.
  The string can be used as part of a url.
  """
  def generate(length) do
    :crypto.strong_rand_bytes(length)
    |> Base.url_encode64()
    |> binary_part(0, length)
  end
end
