defmodule LinkShortener.Generators.LinkWithRandomShorten do
  @moduledoc """
  This module provides a generator for Link.
  """

  alias LinkShortener.Generators.SafeString

  @doc """
  Generate a Link with random shorten with the length.
  """
  def generate_one(attrs, length \\ 10, generator \\ &SafeString.generate/1) do
    Map.put(attrs, :shorten, generator.(length))
  end
end
