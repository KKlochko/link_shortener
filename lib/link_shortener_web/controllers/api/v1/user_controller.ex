defmodule LinkShortenerWeb.Api.V1.UserController do
  use LinkShortenerWeb, :controller

  alias LinkShortener.Accounts
  alias LinkShortener.Accounts.User
  alias LinkShortenerWeb.Auth.Guardian

  action_fallback LinkShortenerWeb.FallbackController

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.register_user(user_params),
	 {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    with {:ok, user, token} <- Guardian.authenticate(email, password) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end
end
