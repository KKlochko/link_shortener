defmodule LinkShortenerWeb.Api.V1.LinkController do
  use LinkShortenerWeb, :controller

  alias LinkShortener.Links
  alias LinkShortener.Links.Link

  action_fallback LinkShortenerWeb.FallbackController

  def index(conn, _params) do
    links = Links.get_all()
    render(conn, "index.json", links: links)
  end

  def create(conn, %{"link" => link_params}) do
    with {:ok, %Link{} = link} <- Links.insert_one(link_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.v1_link_path(conn, :show, link))
      |> render("show.json", link: link)
    end
  end

  def show(conn, %{"id" => id}) do
    link = Links.get_one!(id)
    render(conn, "show.json", link: link)
  end

  def update(conn, %{"id" => id, "link" => link_params}) do
    link = Links.get_one!(id)

    with {:ok, %Link{} = link} <- Links.update_one(link, link_params) do
      render(conn, "show.json", link: link)
    end
  end

  def delete(conn, %{"id" => id}) do
    link = Links.get_one!(id)

    with {:ok, %Link{}} <- Links.delete_one(link) do
      send_resp(conn, :no_content, "")
    end
  end
end
