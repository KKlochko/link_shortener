defmodule LinkShortenerWeb.RedirectController do
  use LinkShortenerWeb, :controller
  alias LinkShortener.Links

  def show(conn, %{"shorten" => shorten}) do
    %{url: url} = Links.get_one_by_shorten(shorten)
    redirect(conn, external: url)
  end
end
