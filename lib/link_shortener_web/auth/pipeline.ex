defmodule LinkShortenerWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :link_shortener,
    module: LinkShortenerWeb.Auth.Guardian,
    error_handler: LinkShortenerWeb.Auth.ErrorHandler

  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
