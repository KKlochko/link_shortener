defmodule LinkShortenerWeb.Api.V1.UserView do
  use LinkShortenerWeb, :view

  alias LinkShortenerWeb.Api.V1.UserView

  def render("user.json", %{user: user, token: token}) do
    %{
      email: user.email,
      token: token
    }
  end
end
